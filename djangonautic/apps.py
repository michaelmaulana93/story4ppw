from django.apps import AppConfig

class myAppNameConfig(AppConfig):
    name = 'djangonautic'
    verbose_name = 'A Much Better Name'