from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from djangonautic.forms import JadwalForm
from django.contrib.auth.models import User
from djangonautic.models import Post

def trackrecord(request):
    #return HttpResponse('track record')
    return render(request, 'trackrecord.html')

def profile(request):
    #return HttpResponse('profile')
    return render(request, 'index.html')

def skills(request):
    return render(request, 'skills.html')

def socialmedia(request):
    return render(request, 'socialmedia.html')

def display(request):
    posts = Post.objects.all()

    args = {'post': posts}
    return render(request, 'display.html', args)

def delete(request):
    Post.objects.all().delete()
    return redirect('display')

#def jadwal(request):
    #return render(request, 'jadwal.html')

class JadwalView(TemplateView):
    template_name ='jadwal.html'

    def get(self, request):
        form = JadwalForm()
        posts = Post.objects.all()

        args = {'form': form}
        return render(request, self.template_name, args)

    def post(self, request):
        form = JadwalForm(request.POST)
        if form.is_valid():
            post = form.save()
            post.save()
            #post.user = request.user
            #post.save()

            text = form.cleaned_data['post']
            form = JadwalForm()
            #return redirect('jadwal')

        args = {'form': form, 'text': text}
        return render(request, self.template_name, args)
