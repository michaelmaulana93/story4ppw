from django import forms
from djangonautic.models import Post

class JadwalForm(forms.ModelForm):
    post = forms.CharField()

    class Meta:
        model = Post
        fields = ('post',)